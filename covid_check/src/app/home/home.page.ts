import { Component } from '@angular/core';
import { SrvcovidService } from '../srvcovid.service';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  summary: any[] = [];
  lastDate: null;
  countries_n: any;

  constructor( public servicio: SrvcovidService, public alert: AlertController){
  }
  async presentAlert() {
    const alert = await this.alert.create({
      header: 'Alert',
      subHeader: 'Element will be erased',
      message: 'This message wont appear again to avoid annoying spam.',
      buttons: ['OK']
    });

    await alert.present();
  }
  ionViewWillEnter(){
    this.servicio.getPage()
    .subscribe(
      (data) =>{
        this.summary = data['Global'];
        this.lastDate = data['Date'];
        this.countries_n = this.getNewData(data['Countries'], this.servicio.getSelectedCountries());
        console.log(this.countries_n);
        console.log('hola?');
      }, (error) =>{
        console.error(error);
      }
    )

  }
  
  getNewData(countries, selected){
    let Data_n = [];
    console.log('entre al for');
    for (let i = 0; i <selected.length;i++){
      console.log('itere en i');
      for(let j = 0; i<countries.length;j++){
        console.log('itere en j');
        if (selected[i]['ISO2'] == countries[j]['CountryCode']){
          
          console.log('entre buen');
          Data_n.push(countries[j]);
          break;
        }
      }
    }
    return Data_n;
  }
  removeCountry(countryCode){
    let data = [];
    this.servicio.getSelectedCountries().forEach(element => {
      if(element['ISO2'] != countryCode){
        data.push(element);
      }
      
    });
    this.servicio.setSelectedCountries(data);
    this.presentAlert();
    
  }

}
