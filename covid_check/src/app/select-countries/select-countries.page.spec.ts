import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectCountriesPage } from './select-countries.page';

describe('SelectCountriesPage', () => {
  let component: SelectCountriesPage;
  let fixture: ComponentFixture<SelectCountriesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectCountriesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectCountriesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
