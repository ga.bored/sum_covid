import { Component, OnInit } from '@angular/core';
import { SrvcovidService } from '../srvcovid.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-select-countries',
  templateUrl: './select-countries.page.html',
  styleUrls: ['./select-countries.page.scss'],
})
export class SelectCountriesPage implements OnInit {

  countries;
  ssk;

  constructor( public service: SrvcovidService, public router: Router, public alert: AlertController) {
  }
  async presentAlert() {
    const alert = await this.alert.create({
      header: 'Alert',
      subHeader: 'Update',
      message: 'Your Countries selection has been updated and it can be seen on the mainscreen',
      buttons: ['OK']
    });

    await alert.present();
  }
  ngOnInit(){
    this.service.getCountries().subscribe(
      (data) => {
      this.countries = data;
      console.log(data);
      this.countries.sort((a,b) => a.Country.toLowerCase() > b.Country.toLowerCase()? 1: -1);
      
      this.ssk = this.service.getSelectedCountries();
      console.log(this.ssk);

      this.ssk.forEach(elemento => {
        if (elemento['isChecked']){
          this.countries.forEach(element => {
            if (element['ISO2'] == elemento['ISO2']){
              element['isChecked'] = true;
              console.log(element);
            }
          });
        }
        
      });
      
      },
      (error) => {
        console.error(error);
      }
      )
    
  }
  onSubmit(){
    let data = [];
  
    this.countries.forEach(element => {
      if (element.isChecked){
        data.push(element);
      }
      
    });
    console.log(data);
    this.service.setSelectedCountries(data);
    this.presentAlert();
    this.router.navigate(['/home']);
    
  }
}

