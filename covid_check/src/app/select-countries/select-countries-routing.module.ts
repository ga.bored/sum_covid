import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectCountriesPage } from './select-countries.page';

const routes: Routes = [
  {
    path: '',
    component: SelectCountriesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectCountriesPageRoutingModule {}
