import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class SrvcovidService {
  selectedCountries = [];
  constructor(private http: HttpClient, private storage: Storage ) {
    this.loadSelectedCountries();
  }
  getPage(){
   return this.http.get('https://api.covid19api.com/summary');
  }
  getCountries(){
    return this.http.get('https://api.covid19api.com/countries');
  }
  getSelectedCountries(){
    return this.selectedCountries;
  }
  setSelectedCountries(countries_n){
    this.selectedCountries = countries_n;
    this.storage.set('cities', this.selectedCountries);
  }
  loadSelectedCountries(){
    this.storage.get('cities').then((val)=>{
      console.log(val);
      if(val != null){
        this.selectedCountries = val;
      }
    }
    );
  }

}
